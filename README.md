#Frontend common docs

Для разработки SPA требующего индексацию и SEO можно использовать текущий пакет
Next - https://gitlab.com/bassam92/telel2-front-common

lodash - использовать для проверок если в проекте большая или средняя команда
Так же прикрепить debounce для Input-ов с 0.5 сек 

Ant design по умолчанию использовать для всех проектов что бы не плодить кучу

Необходимо создать общий common package с компонентами которые переиспользуются для этого советую использовать Storybook

Axios - общий выбор для запросов ( /API - рекомендации по созданию общего шаблона )

Так же Рекомендуем при проектах требующих безопасность хранить токены в куки вместо локального хранилища. 

Typescript при проекте выше лендинга рекомендуется использовать ts.

Пакет сборщик webpack он значительно быстрее и функциональнее rolup & gulp советую его выбрать. 

Покрытие тестами на Jest. https://jestjs.io/ru/docs/getting-started

Для старта верстки при кастомных без подключения ant d рекомендую использовать https://csslayout.io/

# При работе на проекте более 5 ти разработчиков рекомендую работать через Pull request. 
Что бы не только approve or need work а так же ознакомились все с новыми фичами и изменениями.

Настройка eslint и tsconfig индивидуальна для каждой команды.

Рекомендуется настроить Sonarqube при работе через Pull requests. Но прошу отключить проверку на количество задетого кода так как при правке багов будет красный sonarqube.

Так же подключить ci/cd к pull request что бы при запуске pr или обновлении запускался build который бы выводил собралось ли или почему не собралось.

#Technical interview frontend 

Требуется создать три файла (легкий, средний, сложный) устных вопросов и задач.
При собеседовании в зависимости от уровня задавать по определенному количеству задач и вопрос. 
Пример - middle ( 1 легкая задача, 4 средних и 1 тяжелая )

#
# CI_CD_FRONT

1 — Configure the pipeline
In this step, we will be configuring our pipeline using Docker and GitLab CI. Using your preferred text editor or IDE (personally I used VSCode), create a new file named Dockerfile. Example in parent dir.

2 - Create .gitlab-ci.yml 
In parent dir. Example exist. Then commit -m "Pipeline Configurations" and push.

3 - Configure server for gitlab runner ( if it will be required ) 

4 - Install runner 
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner
sudo gitlab-runner start

5 - Register a pipeline
To register a pipeline, run the following command:
sudo gitlab-runner register
You will be returned a prompt like this:
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
Type https://gitlab.com/ and press Enter. The next prompt will be:
Please enter the gitlab-ci token for this runner:
You can get your token from your nextjs-blog project dashboard by hovering on Settings on the navigator section on the left, another sub-menu will show up with a bunch of options, click on CI / CD.
In gitlab left menu (in project) you can find Setting > CI / CD 
Next, click on the Expand button on the runner’s option.
You will see your token after the option is expanded, copy it, go back to your server SSH terminal, paste it and press the Enter key.

Next, you will be asked to enter a description and tags for the runner like this:
Please enter the gitlab-ci description for this runner:
and
Please enter the gitlab-ci tags for this runner (comma separated):
Press the Enter key to skip these. Next, you will be prompted with the following:
Please enter the executor: ssh, virtualbox, kubernetes, docker, docker-ssh, shell, docker+machine, docker-ssh+machine, custom, parallels:
Type in shell and press enter. At this point, you have successfully registered a GitLab Runner on your server.

6 - Install docker ( if not installed ) 
You can quickly install docker by running:
sudo snap install docker
This will quickly install Docker on your machine. Next, we need to set permissions for Docker by running:
sudo groupadd docker
sudo usermod -aG docker $USER
Reboot your server like this:
reboot
Log back in and run:
sudo usermod -aG docker gitlab-runner
This will add GitLab Runner to the Docker group.

7 — Add GitLab Runner to sudoers
To add GitLab Runner to the list of sudoers, run:
sudo nano /etc/sudoers
Paste the following at the end of the file:
gitlab-runner ALL=(ALL) NOPASSWD: ALL
Save and exit nano.

8 - Verify and test
To verify your information run:
sudo nano /etc/gitlab-runner/config.toml
It should contain contents like this:
...
[[runners]]
  url = "https://gitlab.com/"
  token = "XXXXXXXXX"
  executor = "shell"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
...
If it’s not present in the file, add it along with the right credentials, save and exit the file. Restart GitLab Runner:
sudo gitlab-runner restart
Test deployment
In this step, you will be running your pipeline to see if your application will deploy on the server.
every time you push your code on master branch, the pipelines will run automatically.
Once deployment succeeds, you will see a Job succeeded message. If it fails, review all the steps to make sure you did not miss anything.
Visit your server IP address on your browser with port 3000 http://<server-ip>:3000, you will see the NextJS landing page, which means your application was deployed successfully.
Your application will build and deploy itself on every push made to the master branch automatically.

