import {createInstance} from "../index";

export const getMethod = (url, token) =>
    createInstance(token).get(url)
