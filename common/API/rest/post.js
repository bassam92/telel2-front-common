import {createInstance} from '../index'

export const postMethod = (url, data, token) =>
    createInstance(token).post(url, data)
