import axios from 'axios'

export const createInstance = (token) => {

    return axios.create({
        baseURL: 'localhost:3000',
        headers: token && {Authorization: `Bearer ${token}`},
    })

}
